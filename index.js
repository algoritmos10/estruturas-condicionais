console.log("Desvio Condicional if");

console.log("========= ATIVIDADE =========");

function ex1(n1, n2) {
  let maior;
  if (n1 > n2) {
    maior = n1;
  } else {
    maior = n2;
  }
  return maior;
}
console.log(ex1(10, 25));

console.log(".");


function ex2(num) {
  let resposta;
  if (num % 2 == 0) {
    resposta = num + " É par";
  } else {
    resposta = num + " É ímpar";
  }
  return resposta;
}
console.log(ex2(37));

console.log(".");


function ex3(num) {
  let resposta;
  if (num % 2 == 0 && num % 10 == 0) {
    resposta = num + " é par e divisível por 10";
  } else {
    resposta = num + " não é par e/ou não é divisível por 10";
  }
  return resposta;
}
console.log(ex3(40));

console.log(".");

function ex4(num) {
  let resposta;
  if (num % 11 == 0 || num % 13 == 0) {
    resposta = num + " é divisível por 11 ou por 13";
  } else {
    resposta = num + " não é divisível por 11 e por 13";
  }
  return resposta;
}
console.log(ex4(341));

console.log(".");

function ex5(a, b, c) {
  if (a == b && b == c) {
    //equilátero
    console.log("equilátero");
  } else if (a == b || b == c || a == c) {
    //isósceles
    console.log("isósceles");
  } else {
    //escaleno
    console.log("escaleno");
  }
}
ex5(3, 3, 3);
ex5(3, 3, 2);
ex5(3, 4, 5);

console.log(".");


function ex6(nota, frequência) {
  if (nota >= 7 && frequência >= 75) {
    console.log("Aprovado");
  } else if (nota < 7 && frequência >= 75) {
    console.log("Reprovado por nota");
  } else if (nota < 7 && frequência < 75)
    console.log("Reprovado por nota e Frequência");
  else if (nota >= 7 && frequência < 75) {
    console.log("Reprovado por Frequência");
  }
}
ex6(9, 80);
ex6(9, 70);
ex6(5, 55);
ex6(5, 90);

console.log(".");

function ex7(nome, altura, sexo) {
  let peso_ideal;
  if (sexo == "M" || sexo == "m" || sexo == "masculino") {
    peso_ideal = 72.7 * altura - 58;
  } else {
    peso_ideal = 62.1 * altura - 44.7;
  }
  console.log(`${nome} seu peso ideal é ${peso_ideal}`);
}
ex7("José,", 1.9, "M");
ex7("Milena,", 1.67, "F");
